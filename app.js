const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');
const ejs = require('ejs');
const path = require('path');

const app = express();
const port = 3000;

// app.engine('.html', require('ejs').__express);
// app.set('view engine', 'ejs');

app.use(cors());

// app.engine('.html', require('ejs').renderFile);
// app.set('view engine', 'ejs');

app.engine('html', ejs.renderFile);
app.set('view engine', 'html');
app.set('view options', { delimiter: '?' });
app.set('views', path.join(__dirname, 'public'));
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
	return res.render('index');
});

app.use(express.static(path.join(__dirname, 'public')));

// app.use(express.static('public'));

app.listen(port, () => console.log(`Cruce de Mundos listening on port ${port}!`));
