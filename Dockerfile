############################################################
# Dockerfile to build NodeJS 4 Installed Containers
# Based on Node:4.4.3
############################################################

FROM node:11

EXPOSE 3000

# Copy application folder and configurations
COPY . /app

# Create data directory
WORKDIR /app

# NPM INSTALL
RUN npm install

CMD ["node", "app.js"]
